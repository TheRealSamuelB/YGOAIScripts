using Enumerator;
namespace DuelBot.Game.AI.Decks
{
    public class FurHireExecutor : DefaultExecutor
    {
        public static void Init()
        {
            DecksManager.AddDeckType("FurHire", DuelRules.SpeedDuel, (ai, duel) => new FurHireExecutor(ai, duel),
                "E2Go8hNnaIyuZ4Rh/SIzOJbhmcEKw3MKZ4PxWs0HjCDcolrHAMJ6+4pYQXjBf1UWEAaZBwA=");
        }
        public class cardId
        {
            public const int dynaHero = 25123713;
            public const int wizSage = 01527418;
            public const int beatBladesman = 93850652;
            public const int sealStrategist = 20345391;
            public const int reconScout = 31467949;
            public const int donpaMarksman = 94073244;
            public const int mayhem = 91405870;
            public const int cosmicCyclone = 08267140;
            public const int floodGate = 69599136;
        }

        bool usedDonpaEff = false;
        bool usedReconEff = false;

        public FurHireExecutor(GameAI ai, BotDuel duel)
            : base(ai, duel)
        {
            AddExecutor(ExecutorType.Activate, cardId.dynaHero);
            AddExecutor(ExecutorType.Activate, cardId.wizSage);

            AddExecutor(ExecutorType.Summon, cardId.beatBladesman);
            AddExecutor(ExecutorType.Activate, cardId.beatBladesman, beatEff);
            AddExecutor(ExecutorType.Summon, cardId.sealStrategist);
            AddExecutor(ExecutorType.Activate, cardId.sealStrategist, sealEff);
            AddExecutor(ExecutorType.Summon, cardId.donpaMarksman, donpaSummon);
            AddExecutor(ExecutorType.Activate, cardId.donpaMarksman, donpaEff);
            AddExecutor(ExecutorType.Summon, cardId.reconScout, reconSummon);
            AddExecutor(ExecutorType.Activate, cardId.reconScout, reconEff);

            AddExecutor(ExecutorType.Activate, cardId.mayhem, mayhemEff);
            AddExecutor(ExecutorType.SpellSet, cardId.mayhem);

            AddExecutor(ExecutorType.SpellSet, cardId.cosmicCyclone);
            AddExecutor(ExecutorType.Activate, cardId.cosmicCyclone, DefaultCosmicCyclone);
            AddExecutor(ExecutorType.SpellSet, cardId.floodGate);
            AddExecutor(ExecutorType.Activate, cardId.floodGate);
        }
        public override void OnNewTurn()
        {
            if (!Bot.HasInMonstersZone(cardId.donpaMarksman))
            {
                usedDonpaEff = false;
            }
            if (!Bot.HasInMonstersZone(cardId.reconScout))
            {
                usedReconEff = false;
            }
        }

        public override bool OnSelectHand()
        {
            return false;
        }
        public bool beatEff()
        {
            if (!(Bot.HasInMonstersZone(cardId.sealStrategist)))
            {
                AI.SelectCard(cardId.sealStrategist);
            }
            else if (!Bot.HasInHand(cardId.dynaHero))
            {
                AI.SelectCard(cardId.dynaHero);
            }
            else if (Enemy.GetMonsterCount() > 0)
            {
                AI.SelectCard(cardId.donpaMarksman);
            }
            else if (Enemy.GetSpellCount() > 0)
            {
                AI.SelectCard(cardId.reconScout);
            }
            else
            {
                AI.SelectCard(cardId.wizSage);
            }
            return true;
        }
        public bool sealEff()
        {
            if (!Bot.HasInMonstersZone(cardId.beatBladesman)) 
            {
                AI.SelectCard(cardId.beatBladesman);
            }
            else if (Enemy.GetMonsterCount() > 0)
            {
                AI.SelectCard(cardId.donpaMarksman);
            }
            else if (Enemy.GetSpellCount() > 0)
            {
                AI.SelectCard(cardId.reconScout);
            }
            else if (!Bot.HasInMonstersZone(cardId.dynaHero))
            {
                AI.SelectCard(cardId.dynaHero);
            }
            else
            {
                AI.SelectCard(cardId.wizSage);
            }
            return true;
        }
        public bool donpaEff()
        {
            if (usedDonpaEff == false)
            {
                usedDonpaEff = true;
                return true;
            }
            else
            {
                foreach (BotClientCard EnemyMonsters in Enemy.GetMonsters())
                {
                    if (EnemyMonsters.IsFaceup())
                    {
                        AI.SelectCard(EnemyMonsters);
                        return true;
                    }
                }
                foreach (BotClientCard EnemySpells in Enemy.GetSpells())
                {
                    if (EnemySpells.IsFaceup())
                    {
                        AI.SelectCard(EnemySpells);
                        return true;
                    }
                }
            }
            return false;
        }
        public bool donpaSummon()
        {
            return true;
        }
        public bool reconEff()
        {
            if (usedReconEff == false)
            {
                usedReconEff = true;
                return true;
            }
            else
            {
                foreach (BotClientCard EnemyMonsters in Enemy.GetMonsters())
                {
                    if (EnemyMonsters.IsFacedown())
                    {
                        AI.SelectCard(EnemyMonsters);
                        return true;
                    }
                }
                foreach (BotClientCard EnemySpells in Enemy.GetSpells())
                {
                    if (EnemySpells.IsFacedown())
                    {
                        AI.SelectCard(EnemySpells);
                        return true;
                    }
                }
            }
            return false;
        }
        public bool reconSummon()
        {
            return true;
        }
        public bool mayhemEff()
        {
            if (!Bot.HasInHand(cardId.sealStrategist) && !(Bot.HasInMonstersZone(cardId.sealStrategist)))
            {
                AI.SelectCard(cardId.sealStrategist);
            }
            else if (!Bot.HasInHand(cardId.dynaHero))
            {
                AI.SelectCard(cardId.dynaHero);
            }
            else if (Enemy.GetMonsterCount() > 0)
            {
                AI.SelectCard(cardId.donpaMarksman);
            }
            else if (Enemy.GetSpellCount() > 0)
            {
                AI.SelectCard(cardId.reconScout);
            }
            else
            {
                AI.SelectCard(cardId.wizSage);
                AI.SelectPosition(0);
            }
            return true;
        }

    }

}