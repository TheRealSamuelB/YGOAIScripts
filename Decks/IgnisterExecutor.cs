﻿using Enumerator;
using System.Collections.Generic;

namespace DuelBot.Game.AI.Decks
{
    class IgnisterExecutor : DefaultExecutor
    {
        public static void Init()
        {
            DecksManager.AddDeckType("Ignister", DuelRules.MasterDuel, (ai, duel) => new IgnisterExecutor(ai, duel),
                "M2eoNf8Ix+9y2plAePKeJgYYfivqygrDsqzfGWB4d+kXOL4x154FhE8YZjDDsIaaIhMIO++7ywDDaq8ZWOx5pZhgOLI6mUXiXSwjCNstKYTjrVKtzDB8XfQ+a9bDFcze/NWMZ8q/M86QVmHuS5jIzGXiwBr85SwYN8aeYTriosh6/Icv4ymbr8y/b0uzVMpuZgDRAA==");
        }
        public class CardId
        {
            //Monsters
            public const int ParallelExceed = 71278040;
            public const int LadyDebug = 16188701;
            public const int BalancerLord = 8567955;
            public const int Pikari = 16020923;
            public const int Doyon = 88413677;
            public const int Bururu = 42429678;
            public const int Achichi = 15808381;

            //Spells
            public const int LightningStorm = 14532163;
            public const int PotOfDesires = 35261759;
            public const int FoolishBurialGoods = 35726888;
            public const int CynetMining = 57160136;
            public const int PotOfAvarice = 67169062;
            public const int Terraforming = 73628505;
            public const int AIdlereborn = 22933016;
            public const int CalledByTheGrave = 24224830;
            public const int IgnisterAIland = 59054773;

            //Extra
            public const int WindPegasus = 98506199;
            public const int LightDragon = 61399402;
            public const int Arrival = 11738489;
            public const int Darkfluid = 68934651;
            public const int Avramax = 21887175;
            public const int Zeroboros = 66403530;
            public const int AccessCode = 86066372;
            public const int TransCode = 46947713;
            public const int DarkTemplar = 97383507;
            public const int UpdateJammer = 88093706;
            public const int Splashmage = 59859086;
            public const int Cybersewicckid = 52698008;
            public const int LinkDisciple = 32995276;
            public const int Linguriboh = 24842059;
        }

        bool WasPikariSummoned = false;
        bool WasDoyonSummoned = false;
        bool WasBururuSummoned = false;
        bool WasAchichiSummoned = false;

        List<int> AvariceList = new List<int>
        {
            CardId.Linguriboh,
            CardId.Splashmage,
            CardId.LinkDisciple,
            CardId.Cybersewicckid,
            CardId.UpdateJammer,
            CardId.LadyDebug,
            CardId.BalancerLord

        };
        List<int> Junk = new List<int>
        {
            CardId.FoolishBurialGoods,
            CardId.PotOfAvarice,
            CardId.Terraforming,
            CardId.CalledByTheGrave
        };

        List<int> Cyberse = new List<int>
        {
            CardId.Pikari,
            CardId.Doyon,
            CardId.Bururu,
            CardId.Achichi,
            CardId.BalancerLord,
            CardId.LadyDebug,
            CardId.ParallelExceed
        };

        List<int> TransCodeList = new List<int>
        {
            CardId.DarkTemplar,
            CardId.UpdateJammer,
            CardId.Splashmage,
            CardId.Cybersewicckid,
            CardId.Linguriboh,
            CardId.LinkDisciple
        };

        List<int> LinkClimbing = new List<int>
        {
            CardId.Linguriboh,
            CardId.LinkDisciple,
            CardId.Cybersewicckid,
            CardId.Splashmage,
            CardId.UpdateJammer,
            CardId.DarkTemplar,
            CardId.TransCode
        };
        public IgnisterExecutor(GameAI ai, BotDuel duel)
            : base(ai, duel)
        {
            AddExecutor(ExecutorType.Activate, CardId.CalledByTheGrave, DefaultCalledByTheGrave);
            AddExecutor(ExecutorType.Activate, CardId.LightningStorm, stormEff);
            AddExecutor(ExecutorType.Activate, CardId.Terraforming, terraformingEff);
            AddExecutor(ExecutorType.Activate, CardId.CynetMining, CynetEff);
            AddExecutor(ExecutorType.Activate, CardId.PotOfDesires, DefaultPotOfDesires);
            AddExecutor(ExecutorType.Activate, CardId.PotOfAvarice, when_avarice);
            AddExecutor(ExecutorType.Activate, CardId.FoolishBurialGoods, when_foolish);
            AddExecutor(ExecutorType.Activate, CardId.IgnisterAIland, whenFieldSpell);

            AddExecutor(ExecutorType.Summon, CardId.BalancerLord);
            AddExecutor(ExecutorType.Activate, CardId.BalancerLord, BalancerEff);
            AddExecutor(ExecutorType.Summon, CardId.Pikari, PikariSummon);
            AddExecutor(ExecutorType.Activate, CardId.Pikari, PikariEff);
            AddExecutor(ExecutorType.Summon, CardId.Doyon, DoyonSummon);
            AddExecutor(ExecutorType.Activate, CardId.Doyon, DoyonEff);
            AddExecutor(ExecutorType.Summon, CardId.Bururu, BururuSummon);
            AddExecutor(ExecutorType.Activate, CardId.Bururu, BururuEff);
            AddExecutor(ExecutorType.Summon, CardId.Achichi, AchichiSummon);
            AddExecutor(ExecutorType.Activate, CardId.Achichi, AchichiEff);
            AddExecutor(ExecutorType.Summon, CardId.LadyDebug);
            AddExecutor(ExecutorType.Activate, CardId.LadyDebug, DebugEff);

            AddExecutor(ExecutorType.SpSummon, CardId.Linguriboh, KuribohSp);
            AddExecutor(ExecutorType.SpSummon, CardId.LinkDisciple, DiscipleSp);
            AddExecutor(ExecutorType.SpSummon, CardId.Splashmage, MageSp);
            AddExecutor(ExecutorType.Activate, CardId.IgnisterAIland, fieldSpellEff);
            AddExecutor(ExecutorType.Activate, CardId.Splashmage, MageEff);
            AddExecutor(ExecutorType.SpSummon, CardId.DarkTemplar, TemplarSp);
            AddExecutor(ExecutorType.Activate, CardId.ParallelExceed, ParallelEff);
            AddExecutor(ExecutorType.Activate, CardId.DarkTemplar, TemplarEff);
            AddExecutor(ExecutorType.SpSummon, CardId.LightDragon, LightDragonSp);
            AddExecutor(ExecutorType.Activate, CardId.LightDragon, LightDragonEff);
            AddExecutor(ExecutorType.SpSummon, CardId.WindPegasus, PegasusSp);
            AddExecutor(ExecutorType.Activate, CardId.WindPegasus, PegasusEff);

            AddExecutor(ExecutorType.Activate, CardId.Linguriboh, KuribohEff);
            AddExecutor(ExecutorType.SpSummon, CardId.TransCode, TransCodeSp);
            AddExecutor(ExecutorType.Activate, CardId.TransCode, TransCodeEff);
            AddExecutor(ExecutorType.SpSummon, CardId.Darkfluid, DarkfluidSp);
            AddExecutor(ExecutorType.Activate, CardId.Darkfluid);
            AddExecutor(ExecutorType.Activate, CardId.AIdlereborn);

        }
        public override bool OnSelectHand()
        {
            return false;
        }
        public override void OnNewTurn()
        {
            WasAchichiSummoned = false;
            WasBururuSummoned = false;
            WasDoyonSummoned = false;
            WasPikariSummoned = false;
        }
        private bool whenFieldSpell()
        {
            if (Bot.HasInSpellZone(CardId.IgnisterAIland) && Bot.HasInGraveyard(CardId.IgnisterAIland))
            {
                return false;
            }
            if (Bot.HasInHand(CardId.IgnisterAIland))
            {
                return true;
            }
            if (Bot.HasInSpellZone(CardId.IgnisterAIland))
            {
                return false;
            }
            return false;
        }
        private bool fieldSpellEff()
        {
            if (Bot.HasInSpellZone(CardId.IgnisterAIland) && Bot.HasInGraveyard(CardId.IgnisterAIland))
            {
                return false;
            }
            if (!WasPikariSummoned)
            {
                AI.SelectCard(CardId.Pikari);
                WasPikariSummoned = true;
                return true;
            }
            if (!WasDoyonSummoned)
            {
                AI.SelectCard(CardId.Doyon);
                WasDoyonSummoned = true;
                return true;
            }
            if (!WasBururuSummoned)
            {
                AI.SelectCard(CardId.Bururu);
                WasBururuSummoned = true;
                return true;
            }
            if (!WasAchichiSummoned)
            {
                AI.SelectCard(CardId.Achichi);
                WasAchichiSummoned = true;
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool CynetEff()
        {
            if (!Bot.HasInHand(Junk))
            {
                return false;
            }
            AI.SelectCard(Junk);
            if (!Bot.HasInHand(CardId.BalancerLord) && !Bot.HasInMonstersZone(CardId.BalancerLord))
            {
                AI.SelectNextCard(CardId.BalancerLord);
                return true;
            }
            if (!WasPikariSummoned)
            {
                AI.SelectCard(CardId.Pikari);
                return true;
            }
            if (!WasDoyonSummoned)
            {
                AI.SelectCard(CardId.Doyon);
                return true;
            }
            if (!WasBururuSummoned)
            {
                AI.SelectCard(CardId.Bururu);
                return true;
            }
            if (!WasAchichiSummoned)
            {
                AI.SelectCard(CardId.Achichi);
                return true;
            }
            return false;
        }
        private bool when_avarice()
        {
            if (Bot.Graveyard.Count > 9)
            {
                AI.SelectCard(AvariceList);
                return true;
            }
            return false;
        }
        private bool when_foolish()
        {
            if (!Bot.HasInHandOrInSpellZone(CardId.IgnisterAIland))
            {
                AI.SelectCard(CardId.IgnisterAIland);
                return true;
            }
            return false;
        }
        private bool terraformingEff()
        {
            if (!Bot.HasInHandOrInSpellZone(CardId.IgnisterAIland))
            {
                return true;
            }
            return false;
        }
        private bool stormEff()
        {
            return true;
        }
        private bool DebugEff()
        {
            if (Bot.HasInHand(CardId.Bururu))
            {
                AI.SelectCard(CardId.Achichi);
                return true;
            }
            if (Bot.HasInHand(CardId.Achichi))
            {
                AI.SelectCard(CardId.Bururu);
                return true;
            }
            else
            {
                return true;
            }    
        }
        private bool PikariSummon()
        {
            WasPikariSummoned = true;
            return true;
        }
        private bool DoyonSummon()
        {
            WasDoyonSummoned = true;
            return true;
        }
        private bool BururuSummon()
        {
            WasBururuSummoned = true;
            return true;
        }
        private bool AchichiSummon()
        {
            WasAchichiSummoned = true;
            return true;
        }
        private bool PikariEff()
        {
            if (!Bot.HasInHandOrInSpellZone(CardId.IgnisterAIland))
            {
                AI.SelectCard(CardId.IgnisterAIland);
                return true;
            }
            else
            {
                AI.SelectCard(CardId.AIdlereborn);
                return true;
            }
        }
        private bool DoyonEff()
        {
            if (Bot.HasInGraveyard(CardId.Achichi) && !Bot.HasInHand(CardId.Achichi))
            {
                AI.SelectCard(CardId.Achichi);
                return true;
            }
            if (Bot.HasInGraveyard(CardId.Pikari) && !Bot.HasInHand(CardId.Pikari))
            {
                AI.SelectCard(CardId.Pikari);
                return true;
            }
            else
            {
                return true;
            }
        }
        private bool BururuEff()
        {
            if (!Bot.HasInGraveyard(CardId.Pikari))
            {
                AI.SelectCard(CardId.Pikari);
                return true;
            }
            if (!Bot.HasInGraveyard(CardId.Doyon))
            {
                AI.SelectCard(CardId.Doyon);
                return true;
            }
            if (!Bot.HasInGraveyard(CardId.Achichi))
            {
                AI.SelectCard(CardId.Achichi);
                return true;
            }
            return false;
            
        }
        private bool AchichiEff()
        {
            if (Bot.HasInGraveyard(CardId.Achichi))
            {
                return false;
            }
            if (!Bot.HasInHandOrInMonstersZoneOrInGraveyard(CardId.Doyon))
            {
                AI.SelectCard(CardId.Doyon);
                return true;
            }
            else if (!Bot.HasInHandOrInMonstersZoneOrInGraveyard(CardId.Bururu))
            {
                AI.SelectCard(CardId.Bururu);
                return true;
            }
            else
            {
                AI.SelectCard(CardId.Pikari);
                return true;
            }
        }
        private bool BalancerEff()
        {
            if (Bot.HasInHand(Cyberse))
            {
                return true;
            }
            return false;
        }
        private bool ParallelEff()
        {
            if (Bot.HasInMonstersZone(CardId.DarkTemplar))
            {
                return true;
            }
            return false;
        }
        private bool KuribohSp()
        {
            if (Bot.HasInMonstersZone(Cyberse) && !Bot.HasInMonstersZone(LinkClimbing))
            {
                return true;
            }
            return false;
        }
        private bool KuribohEff()
        {
            return false;
        }
        private bool DiscipleSp()
        {
            if (Bot.HasInMonstersZone(CardId.DarkTemplar) || Bot.HasInMonstersZone(CardId.Darkfluid))
            {
                return false;
            }
            if (Bot.HasInMonstersZone(Cyberse) && !Bot.HasInMonstersZone(LinkClimbing))
            {
                return true;
            }
            return false;
        }
        private bool MageSp()
        {
            if (Bot.HasInMonstersZone(CardId.Linguriboh) || Bot.HasInMonstersZone(CardId.LinkDisciple))
            {
                return true;
            }
            return false;
        }
        private bool MageEff()
        {
            AI.SelectCard(CardId.Bururu);
            return true;
        }
        private bool TemplarSp()
        {
            if (Bot.HasInMonstersZone(CardId.Splashmage) && !Bot.HasInMonstersZone(CardId.DarkTemplar))
            {
                AI.SelectCard(LinkClimbing);
                return true;
            }
            return false;
        }
        private bool TemplarEff()
        {
            AI.SelectCard(Cyberse);
            return true;
        }
        private bool LightDragonSp()
        {
            if (Bot.HasInMonstersZone(CardId.DarkTemplar))
            {
                return true;
            }
            return false;
        }
        private bool LightDragonEff()
        {
            AI.SelectCard(CardId.Splashmage);
            return true;
        }
        private bool PegasusSp()
        {
            if (Bot.HasInMonstersZone(CardId.DarkTemplar))
            {
                return true;
            }
            return false;
        }
        private bool PegasusEff()
        {
            return true;
        }
        private bool TransCodeSp()
        {
            if (Bot.HasInMonstersZone(CardId.Splashmage) && Bot.HasInMonstersZone(Cyberse))
            {
                AI.SelectCard(CardId.Splashmage);
                AI.SelectNextCard(Cyberse);
                return true;
            }
            return false;
        }
        private bool TransCodeEff()
        {
            AI.SelectCard(TransCodeList);
            return true;
        }
        private bool DarkfluidSp()
        {
            if (Bot.HasInMonstersZone(CardId.LightDragon) && Bot.HasInMonstersZone(CardId.WindPegasus) && Bot.HasInMonstersZone(CardId.DarkTemplar))
            {
                AI.SelectCard(CardId.LightDragon);
                AI.SelectNextCard(CardId.WindPegasus);
                AI.SelectThirdCard(CardId.DarkTemplar);
                return true;
            }
            return false;
        }
    }
}