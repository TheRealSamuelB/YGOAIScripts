using Enumerator;
using System;
namespace DuelBot.Game.AI.Decks
{
    public class ManualTestExecutor : DefaultExecutor
    {
        public static void Init()
        {
            DecksManager.AddDeckType("Manual Test", DuelRules.Manual, (ai, duel) => new ManualTestExecutor(ai, duel),
                "02CIsH7J6lcgywDDH0RdGGH4VLIpMwwzVd5mgeFVgScZYNiq4g0jDKd9LmeGYakQa1YYbtdkY4DhY8dnwPHz9ChGEK5xZGSFYYkoaSYQ3sC6nxWEQe4DAA==");
        }
        private static readonly byte[] ToEndPhase = new byte[4] { (byte)CtosMessage.Response, (byte)GameMessage.NewPhase, BitConverter.GetBytes((short)DuelPhase.End)[0], BitConverter.GetBytes((short)DuelPhase.End)[1] };
        private static readonly byte[] ToDrawPhase = new byte[4] { (byte)CtosMessage.Response, (byte)GameMessage.NewPhase, BitConverter.GetBytes((short)DuelPhase.Draw)[0], BitConverter.GetBytes((short)DuelPhase.Draw)[1] };
        private static readonly byte[] ConfirmPrompt = new byte[3] { (byte)CtosMessage.Response, (byte)GameMessage.SelectYesNo, 1 };
        public ManualTestExecutor(GameAI ai, BotDuel duel)
            : base(ai, duel)
        {

        }
        public override void OnNewPhase()
        {
            if (!Util.IsMyTurn() && Util.IsEndPhase())
                Util.SendPacket(ToDrawPhase);
        }
        public override void OnNewTurn()
        {
            if (Util.IsMyTurn())
                Util.SendPacket(ToEndPhase);
        }
        public override bool OnSelectYesNo(int desc)
        {
            Util.SendPacket(ConfirmPrompt);
            return true;
        }
        public override bool OnSelectHand()
        {
            return false;
        }
        public override byte OnRockPaperScissors()
        {
            byte res = (byte)BotCore.Rand.Next(1, 4);
            Util.SendPacket(new byte[3] { (byte)CtosMessage.Response, (byte)GameMessage.HandResult, res });
            return res;
        }
    }
}
