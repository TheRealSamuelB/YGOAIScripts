using Enumerator;

namespace DuelBot.Game.AI.Decks
{
    public class PhotonExecutor : DefaultExecutor
    {
        public static void Init()
        {
            DecksManager.AddDeckType("Photon", DuelRules.SpeedDuel, (ai, duel) => new PhotonExecutor(ai, duel),
                "E2foZZrGCsJ7cp8yw/DcUyIM8vGTmGC4++ITVhj23xjEBMPzQ/wYQFjw8kX2Bf7eLDDM7JfBBMIgcwE=");
        }

        public class CardId
        {
            //Monster cards
            public const int PhotonDragon = 93717133;
            public const int PhotonLizard = 38973775;
            public const int PhotonCrusher = 1362589;
            public const int PhotonThrasher = 65367484;
            public const int PhotonVanisher = 43147039;
            public const int PhotonAdvancer = 98881931;

            // Magic & Trap cards Back off Wizards
            public const int PhotonStreamofDestruction = 72044448;
            public const int CosmicCyclone = 5133471;

            // Extra Deck
            public const int StarligeLordGalaxion = 40390147;

            //skill card
            public const int GalaxyPhoton = 131191569;
        }

        public PhotonExecutor(GameAI ai, BotDuel duel)
            : base(ai, duel)
        {
            // Monster Summons
            AddExecutor(ExecutorType.Summon, CardId.PhotonLizard);
            AddExecutor(ExecutorType.Activate, CardId.PhotonLizard, LizardEff);
            AddExecutor(ExecutorType.SpSummon, CardId.PhotonThrasher, ThrasherSpSummon);
            AddExecutor(ExecutorType.Summon, CardId.PhotonCrusher);
            AddExecutor(ExecutorType.Summon, CardId.PhotonAdvancer, AdvancerSummon);
            AddExecutor(ExecutorType.SpSummon, CardId.PhotonVanisher, VanisherSpSummon);
            AddExecutor(ExecutorType.Activate, CardId.PhotonVanisher, VanisherEff);
            AddExecutor(ExecutorType.SpSummon, CardId.PhotonAdvancer, AdvancerSpSummon);

            // Extra Deck
            AddExecutor(ExecutorType.SpSummon, CardId.StarligeLordGalaxion, GalaxionSpSummon);
            AddExecutor(ExecutorType.Activate, CardId.StarligeLordGalaxion, GalaxionEff);

            // Spells
            AddExecutor(ExecutorType.Activate, CardId.PhotonStreamofDestruction, DestructionEff);
            AddExecutor(ExecutorType.Activate, CardId.CosmicCyclone, DefaultCosmicCyclone);
            AddExecutor(ExecutorType.Activate, CardId.GalaxyPhoton, GalaxyPhotonEff);
            AddExecutor(ExecutorType.SpellSet, CardId.PhotonStreamofDestruction);
        }

        public override bool OnSelectHand()
        {
            return true;
        }

        public override bool OnPreBattleBetween(BotClientCard attacker, BotClientCard defender)
        {
            if (defender.IsAttack() && attacker.Attack > defender.Attack)
            {
                return true;
            }
            if (defender.IsDefense() && attacker.Attack > defender.Defense)
            {
                return true;
            }
            return false;
        }

        private bool ThrasherSpSummon()
        {
            return true;
        }

        private bool VanisherSpSummon()
        {
            return true;
        }
        private bool AdvancerSpSummon()
        {
            return true;
        }

        private bool VanisherEff()
        {
            AI.SelectCard(CardId.PhotonDragon);
            return true;
        }

        private bool LizardEff()
        {
            if (Bot.GetMonsterCount() == 0)
            {
                AI.SelectCard(CardId.PhotonThrasher);
                return true;
            }
            else
            {
                AI.SelectCard(CardId.PhotonAdvancer, CardId.PhotonVanisher);
                return true;
            }
        }

        private bool GalaxionEff()
        {
            AI.SelectOption(1);
            AI.SelectCard(CardId.PhotonDragon);
            return true;
        }

        private bool GalaxionSpSummon()
        {
            AI.SelectCard(
                CardId.PhotonCrusher,
                CardId.PhotonThrasher,
                CardId.PhotonVanisher,
                CardId.PhotonAdvancer
            );
            return true;
        }

        private bool DestructionEff()
        {
            BotClientCard target = Util.GetBestEnemyCard();
            if (target != null)
            {
                AI.SelectCard(target);
                return true;
            }
            return false;
        }

        private bool AdvancerSummon()
        {
            return true;
        }

        private bool GalaxyPhotonEff()
        {
            if (Bot.HasInHand(CardId.PhotonDragon))
            {
                AI.SelectCard(CardId.PhotonDragon);
                AI.SelectNextCard(CardId.PhotonStreamofDestruction);
                return true;
            }
            return false;
        }
    }
}
