using Enumerator;
using System.Collections.Generic;

namespace DuelBot.Game.AI.Decks
{
    public class QliphortExecutor : DefaultExecutor
    {
        public static void Init()
        {
            DecksManager.AddDeckType("Qliphort", DuelRules.MasterDuel, (ai, duel) => new QliphortExecutor(ai, duel),
                "M2cQ3vWcGYZ31R5nAGHlM1ms6zY7M4FwdVoVKwg/3qDFvGN7PxhHPD/IAsOPzR6ygvA7NV5GEBa6uYAJhn/PMmSG4YSS7ywwLOk8lxWGb7pHMixMe8cCw7enZjOB8Nral6wgvD3nNAMMB33awQLDHpcnMMLwlxkqTDBscT6REYYB");
        }
        public class CardId
        {
            public const int Scout = 65518099;
            public const int Stealth = 13073850;
            public const int Shell = 90885155;
            public const int Helix = 37991342;
            public const int Carrier = 91907707;

            public const int DarkHole = 53129443;
            public const int CardOfDemise = 59750328;
            public const int SummonersArt = 79816536;
            public const int PotOfDuality = 98645731;
            public const int Saqlifice = 17639150;

            public const int MirrorForce = 44095762;
            public const int TorrentialTribute = 53582587;
            public const int DimensionalBarrier = 83326048;
            public const int CompulsoryEvacuationDevice = 94192409;
            public const int VanitysEmptiness = 5851097;
            public const int SkillDrain = 82732705;
            public const int SolemnStrike = 40605147;
            public const int TheHugeRevolutionIsOver = 99188141;
        }

        private bool CardOfDemiseUsed = false;
        private readonly IList<int> LowScaleCards = new[]
        {
            CardId.Stealth,
            CardId.Carrier
        };
        private readonly IList<int> HighScaleCards = new[]
        {
            CardId.Scout,
            CardId.Shell,
            CardId.Helix
        };

        public QliphortExecutor(GameAI ai, BotDuel duel)
            : base(ai, duel)
        {

            AddExecutor(ExecutorType.Activate, CardId.DarkHole, DefaultDarkHole);
            AddExecutor(ExecutorType.Activate, CardId.SummonersArt);

            AddExecutor(ExecutorType.Activate, CardId.Scout, ScoutActivate);
            AddExecutor(ExecutorType.Activate, CardId.Scout, ScoutEffect);

            AddExecutor(ExecutorType.Activate, CardId.Stealth, ScaleActivate);
            AddExecutor(ExecutorType.Activate, CardId.Shell, ScaleActivate);
            AddExecutor(ExecutorType.Activate, CardId.Helix, ScaleActivate);
            AddExecutor(ExecutorType.Activate, CardId.Carrier, ScaleActivate);

            AddExecutor(ExecutorType.Summon, NormalSummon);
            AddExecutor(ExecutorType.SpSummon);

            AddExecutor(ExecutorType.Activate, CardId.Saqlifice, SaqlificeEffect);

            AddExecutor(ExecutorType.Activate, CardId.Stealth, StealthEffect);
            AddExecutor(ExecutorType.Activate, CardId.Helix, HelixEffect);
            AddExecutor(ExecutorType.Activate, CardId.Carrier, CarrierEffect);

            AddExecutor(ExecutorType.SpellSet, CardId.SkillDrain, TrapSetUnique);
            AddExecutor(ExecutorType.SpellSet, CardId.VanitysEmptiness, TrapSetUnique);
            AddExecutor(ExecutorType.SpellSet, CardId.DimensionalBarrier, TrapSetUnique);
            AddExecutor(ExecutorType.SpellSet, CardId.TorrentialTribute, TrapSetUnique);
            AddExecutor(ExecutorType.SpellSet, CardId.SolemnStrike, TrapSetUnique);
            AddExecutor(ExecutorType.SpellSet, CardId.MirrorForce, TrapSetUnique);
            AddExecutor(ExecutorType.SpellSet, CardId.CompulsoryEvacuationDevice, TrapSetUnique);
            AddExecutor(ExecutorType.SpellSet, CardId.TheHugeRevolutionIsOver, TrapSetUnique);

            AddExecutor(ExecutorType.SpellSet, CardId.Saqlifice, TrapSetWhenZoneFree);
            AddExecutor(ExecutorType.SpellSet, CardId.SkillDrain, TrapSetWhenZoneFree);
            AddExecutor(ExecutorType.SpellSet, CardId.VanitysEmptiness, TrapSetWhenZoneFree);
            AddExecutor(ExecutorType.SpellSet, CardId.DimensionalBarrier, TrapSetWhenZoneFree);
            AddExecutor(ExecutorType.SpellSet, CardId.TorrentialTribute, TrapSetWhenZoneFree);
            AddExecutor(ExecutorType.SpellSet, CardId.SolemnStrike, TrapSetWhenZoneFree);
            AddExecutor(ExecutorType.SpellSet, CardId.MirrorForce, TrapSetWhenZoneFree);
            AddExecutor(ExecutorType.SpellSet, CardId.CompulsoryEvacuationDevice, TrapSetWhenZoneFree);
            AddExecutor(ExecutorType.SpellSet, CardId.TheHugeRevolutionIsOver, TrapSetWhenZoneFree);
            AddExecutor(ExecutorType.SpellSet, CardId.DarkHole, TrapSetWhenZoneFree);
            AddExecutor(ExecutorType.SpellSet, CardId.SummonersArt, TrapSetWhenZoneFree);
            AddExecutor(ExecutorType.SpellSet, CardId.PotOfDuality, TrapSetWhenZoneFree);

            AddExecutor(ExecutorType.Activate, CardId.PotOfDuality, PotOfDualityEffect);
            AddExecutor(ExecutorType.SpellSet, CardId.CardOfDemise);
            AddExecutor(ExecutorType.Activate, CardId.CardOfDemise, CardOfDemiseEffect);

            AddExecutor(ExecutorType.SpellSet, CardId.Saqlifice, CardOfDemiseAcivated);
            AddExecutor(ExecutorType.SpellSet, CardId.SkillDrain, CardOfDemiseAcivated);
            AddExecutor(ExecutorType.SpellSet, CardId.VanitysEmptiness, CardOfDemiseAcivated);
            AddExecutor(ExecutorType.SpellSet, CardId.DimensionalBarrier, CardOfDemiseAcivated);
            AddExecutor(ExecutorType.SpellSet, CardId.TorrentialTribute, CardOfDemiseAcivated);
            AddExecutor(ExecutorType.SpellSet, CardId.SolemnStrike, CardOfDemiseAcivated);
            AddExecutor(ExecutorType.SpellSet, CardId.MirrorForce, CardOfDemiseAcivated);
            AddExecutor(ExecutorType.SpellSet, CardId.CompulsoryEvacuationDevice, CardOfDemiseAcivated);
            AddExecutor(ExecutorType.SpellSet, CardId.TheHugeRevolutionIsOver, CardOfDemiseAcivated);
            AddExecutor(ExecutorType.SpellSet, CardId.DarkHole, CardOfDemiseAcivated);
            AddExecutor(ExecutorType.SpellSet, CardId.SummonersArt, CardOfDemiseAcivated);
            AddExecutor(ExecutorType.SpellSet, CardId.PotOfDuality, CardOfDemiseAcivated);

            AddExecutor(ExecutorType.Activate, CardId.TheHugeRevolutionIsOver, DefaultTrap);
            AddExecutor(ExecutorType.Activate, CardId.SolemnStrike, DefaultSolemnStrike);
            AddExecutor(ExecutorType.Activate, CardId.SkillDrain, SkillDrainEffect);
            AddExecutor(ExecutorType.Activate, CardId.VanitysEmptiness, DefaultUniqueTrap);
            AddExecutor(ExecutorType.Activate, CardId.CompulsoryEvacuationDevice, DefaultCompulsoryEvacuationDevice);
            AddExecutor(ExecutorType.Activate, CardId.DimensionalBarrier, DefaultDimensionalBarrier);
            AddExecutor(ExecutorType.Activate, CardId.MirrorForce, DefaultUniqueTrap);
            AddExecutor(ExecutorType.Activate, CardId.TorrentialTribute, DefaultTorrentialTribute);

            AddExecutor(ExecutorType.Repos, DefaultMonsterRepos);
        }

        public override bool OnSelectHand()
        {
            return true;
        }

        public override void OnNewTurn()
        {
            CardOfDemiseUsed = false;
        }

        public override IList<BotClientCard> OnSelectPendulumSummon(IList<BotClientCard> cards, int max)
        {
            UnityEngine.Debug.Log("OnSelectPendulumSummon");
            // select the last cards

            IList<BotClientCard> selected = new List<BotClientCard>();
            for (int i = 1; i <= max; ++i)
            {
                BotClientCard card = cards[cards.Count - i];
                if (!card.IsCode(CardId.Scout) || (card.Location == CardLocation.Extra && !BotDuel.IsNewRule))
                {
                    selected.Add(card);
                }
            }
            if (selected.Count == 0)
            {
                selected.Add(cards[cards.Count - 1]);
            }

            return selected;
        }

        private bool NormalSummon()
        {
            if (DynamicCard.IsCode(CardId.Scout))
            {
                return false;
            }

            if (DynamicCard.Level < 8)
            {
                AI.SelectOption(1);
            }

            return true;
        }

        private bool SkillDrainEffect()
        {
            return (Bot.LifePoints > 1000) && DefaultUniqueTrap();
        }

        private bool PotOfDualityEffect()
        {
            AI.SelectCard(
                CardId.Scout,
                CardId.SkillDrain,
                CardId.VanitysEmptiness,
                CardId.DimensionalBarrier,
                CardId.Stealth,
                CardId.Shell,
                CardId.Helix,
                CardId.Carrier,
                CardId.SolemnStrike,
                CardId.CardOfDemise
                );
            return !ShouldPendulum();
        }

        private bool CardOfDemiseEffect()
        {
            if (Util.IsTurn1OrMain2() && !ShouldPendulum())
            {
                CardOfDemiseUsed = true;
                return true;
            }
            return false;
        }

        private bool TrapSetUnique()
        {
            foreach (BotClientCard card in Bot.GetSpells())
            {
                if (card.IsCode(DynamicCard.Id))
                {
                    return false;
                }
            }
            return TrapSetWhenZoneFree();
        }

        private bool TrapSetWhenZoneFree()
        {
            return Bot.GetSpellCountWithoutField() < 4;
        }

        private bool CardOfDemiseAcivated()
        {
            return CardOfDemiseUsed;
        }

        private bool SaqlificeEffect()
        {
            if (DynamicCard.Location == CardLocation.Grave)
            {
                BotClientCard l = Util.GetPZone(0, 0);
                BotClientCard r = Util.GetPZone(0, 1);
                if (l == null && r == null)
                {
                    AI.SelectCard(CardId.Scout);
                }
            }
            return true;
        }

        private bool ScoutActivate()
        {
            if (DynamicCard.Location != CardLocation.Hand)
            {
                return false;
            }

            BotClientCard l = Util.GetPZone(0, 0);
            BotClientCard r = Util.GetPZone(0, 1);
            if (l == null && r == null)
            {
                return true;
            }

            if (l == null && r.RScale != DynamicCard.LScale)
            {
                return true;
            }

            if (r == null && l.LScale != DynamicCard.RScale)
            {
                return true;
            }

            return false;
        }

        private bool ScaleActivate()
        {
            if (!DynamicCard.HasType(CardType.Pendulum) || DynamicCard.Location != CardLocation.Hand)
            {
                return false;
            }

            int count = 0;
            foreach (BotClientCard card in Bot.Hand.GetMonsters())
            {
                if (!DynamicCard.Equals(card))
                {
                    count++;
                }
            }
            foreach (BotClientCard card in Bot.ExtraDeck.GetFaceupPendulumMonsters())
            {
                count++;
            }
            BotClientCard l = Util.GetPZone(0, 0);
            BotClientCard r = Util.GetPZone(0, 1);
            if (l == null && r == null)
            {
                if (CardOfDemiseUsed)
                {
                    return true;
                }

                bool pair = false;
                foreach (BotClientCard card in Bot.Hand.GetMonsters())
                {
                    if (card.RScale != DynamicCard.LScale)
                    {
                        pair = true;
                        count--;
                        break;
                    }
                }
                return pair && count > 1;
            }
            if (l == null && r.RScale != DynamicCard.LScale)
            {
                return count > 1 || CardOfDemiseUsed;
            }

            if (r == null && l.LScale != DynamicCard.RScale)
            {
                return count > 1 || CardOfDemiseUsed;
            }

            return false;
        }

        private bool ScoutEffect()
        {
            if (DynamicCard.Location == CardLocation.Hand)
            {
                return false;
            }

            int count = 0;
            int handcount = 0;
            int fieldcount = 0;
            foreach (BotClientCard card in Bot.Hand.GetMonsters())
            {
                count++;
                handcount++;
            }
            foreach (BotClientCard card in Bot.MonsterZone.GetMonsters())
            {
                fieldcount++;
            }
            foreach (BotClientCard card in Bot.ExtraDeck.GetFaceupPendulumMonsters())
            {
                count++;
            }
            if (count > 0 && !Bot.HasInHand(LowScaleCards))
            {
                AI.SelectCard(LowScaleCards);
            }
            else if (handcount > 0 || fieldcount > 0)
            {
                AI.SelectCard(CardId.Saqlifice, CardId.Shell, CardId.Helix);
            }
            else
            {
                AI.SelectCard(HighScaleCards);
            }
            return Bot.LifePoints > 800;
        }

        private bool StealthEffect()
        {
            if (DynamicCard.Location == CardLocation.Hand)
            {
                return false;
            }

            BotClientCard target = Util.GetBestEnemyCard();
            if (target != null)
            {
                AI.SelectCard(target);
                return true;
            }
            return false;
        }

        private bool CarrierEffect()
        {
            if (DynamicCard.Location == CardLocation.Hand)
            {
                return false;
            }

            BotClientCard target = Util.GetBestEnemyMonster();
            if (target != null)
            {
                AI.SelectCard(target);
                return true;
            }
            return false;
        }

        private bool HelixEffect()
        {
            if (DynamicCard.Location == CardLocation.Hand)
            {
                return false;
            }

            BotClientCard target = Util.GetBestEnemySpell();
            if (target != null)
            {
                AI.SelectCard(target);
                return true;
            }
            return false;
        }

        private bool ShouldPendulum()
        {
            BotClientCard l = Util.GetPZone(0, 0);
            BotClientCard r = Util.GetPZone(0, 1);
            if (l != null && r != null && l.LScale != r.RScale)
            {
                int count = 0;
                foreach (BotClientCard card in Bot.Hand.GetMonsters())
                {
                    count++;
                }
                foreach (BotClientCard card in Bot.ExtraDeck.GetFaceupPendulumMonsters())
                {
                    count++;
                }
                return count > 1;
            }
            return false;
        }

    }
}