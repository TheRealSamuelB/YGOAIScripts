﻿using Enumerator;
using System.Collections.Generic;
using System.Linq;
using System;

namespace DuelBot.Game.AI.Decks.Rush
{
    public class DinocarriageDynarmixExecutor : DefaultExecutor
    {
        public static void Init()
        {
            DecksManager.AddDeckType("DinocarriageDynarmix", DuelRules.RushDuel, (ai, duel) => new RushExecutor(ai, duel),
                "02C4F63JDsP3kfBdJPyWE4GvQvGTaAR+ioQfQ/FDJPwICb9Cws+h+CUSfgHEb6D4NdRdAA==");
        }
        public class CardId
        {
            public const int 超级恐龙王 = 120130029;
            public const int 巨身多角龙 = 120130005;
            public const int 荒野盗龙 = 120151012;
            public const int 名流雷龙 = 120151013;
            public const int 抑龙 = 120151011;
            public const int 机械镰刀盗龙 = 120151009;
            public const int 成金恐龙王 = 120151010;
            public const int 侏罗纪世界 = 120151015;
            public const int 奇迹的共进化 = 120151016;
            public const int 成金哥布林 = 120151018;
            public const int 恐龙粉碎 = 120151017;
            public const int 超越进化 = 120151019;
            public const int 恐力重压 = 120151020;
            public const int 大恐龙驾L = 120151005;
            public const int 大恐龙驾R = 120151007;
            public const int 大恐龙驾 = 120151006;
        }

        public DinocarriageDynarmixExecutor(GameAI ai, BotDuel duel)
            : base(ai, duel)
        {
            AddExecutor(ExecutorType.SpSummon, CardId.大恐龙驾, MAXsummon);
            AddExecutor(ExecutorType.Activate, CardId.大恐龙驾, 大恐龙驾Effect);
            AddExecutor(ExecutorType.Activate, CardId.成金哥布林);
            AddExecutor(ExecutorType.Activate, CardId.抑龙);
            AddExecutor(ExecutorType.Activate, CardId.成金恐龙王, 成金恐龙王Effect);
            AddExecutor(ExecutorType.Activate, CardId.机械镰刀盗龙, 机械镰刀盗龙Effect);
            AddExecutor(ExecutorType.Activate, CardId.奇迹的共进化, 奇迹的共进化Effect);
            AddExecutor(ExecutorType.Summon, CardId.巨身多角龙, DefaultMonsterSummon);
            AddExecutor(ExecutorType.Activate, CardId.超级恐龙王, 超级恐龙王Effect);
            AddExecutor(ExecutorType.Summon, CardId.机械镰刀盗龙);
            AddExecutor(ExecutorType.Summon, CardId.抑龙, 抑龙Summon);
            AddExecutor(ExecutorType.Summon, CardId.成金恐龙王, 成金恐龙王Summon);
            AddExecutor(ExecutorType.Activate, CardId.恐龙粉碎, 恐龙粉碎Effect);
            AddExecutor(ExecutorType.SummonOrSet, CardId.荒野盗龙, DefaultMonsterSummon);
            AddExecutor(ExecutorType.SummonOrSet, CardId.名流雷龙, DefaultMonsterSummon);
            AddExecutor(ExecutorType.Summon, CardId.超级恐龙王, DefaultMonsterSummon);
            AddExecutor(ExecutorType.Summon, CardId.大恐龙驾L, MAXLsummon);
            AddExecutor(ExecutorType.Summon, CardId.大恐龙驾R, MAXRsummon);
            AddExecutor(ExecutorType.MonsterSet, CardId.大恐龙驾L, MAXLset);
            AddExecutor(ExecutorType.MonsterSet, CardId.大恐龙驾R, MAXRset);
            AddExecutor(ExecutorType.MonsterSet, CardId.抑龙);
            AddExecutor(ExecutorType.MonsterSet, CardId.成金恐龙王);
            AddExecutor(ExecutorType.MonsterSet, CardId.名流雷龙, monsterset);
            AddExecutor(ExecutorType.Repos, DefaultMonsterRepos);
            AddExecutor(ExecutorType.Activate, CardId.侏罗纪世界);
            AddExecutor(ExecutorType.Activate, CardId.恐力重压);
            AddExecutor(ExecutorType.Activate, CardId.超越进化, 超越进化Effect);
            AddExecutor(ExecutorType.SpellSet);            
            //AddExecutor(ExecutorType.Activate, DefaultDontChainMyself);
        }

        private bool 大恐龙驾Effect()
        {
            {            
                List<BotClientCard> targets = new();
                foreach (var target in Enemy.GetMonsters())
                {
                    if (target.IsFacedown())
                        targets.Add(target);
                    if (targets.Count >= 2)
                        break;
                }
                if (targets.Count < 2)
                {
                    foreach (var target in Enemy.GetMonsters())
                    {
                        if (target.IsFaceup())
                            targets.Add(target);
                        if (targets.Count >= 2)
                            break;
                    }
                }
                if (targets.Count > 0)
                {
                    AI.SelectCard(
                     CardId.抑龙,
                     CardId.名流雷龙,
                     CardId.荒野盗龙,
                     CardId.巨身多角龙,
                     CardId.成金恐龙王,
                     CardId.机械镰刀盗龙,
                     CardId.超级恐龙王,
                     CardId.大恐龙驾,
                     CardId.大恐龙驾L,
                     CardId.大恐龙驾R
                     );
                    AI.SelectNextCard(targets);
                    return true;
                }
                return false;
            }
        }
        private bool MAXsummon()
        {
            if (Bot.HasInMonstersZone(CardId.大恐龙驾))
            {
                return false;
            }
            else
            AI.SelectCard(CardId.大恐龙驾L);
            AI.SelectNextCard(CardId.大恐龙驾R);
            return true;
        }

        private List<DHint> DHintForEnemy = new()
        {
            DHint.RELEASE, DHint.DESTROY, DHint.REMOVE, DHint.TOGRAVE, DHint.RTOHAND, DHint.TODECK,
            DHint.FMATERIAL, DHint.SMATERIAL, DHint.XMATERIAL, DHint.LMATERIAL, DHint.DISABLEZONE
        };
        private List<DHint> DHintForMaxSelect = new()
        {
            DHint.SPSUMMON, DHint.TOGRAVE, DHint.ATOHAND, DHint.TODECK, DHint.DESTROY
        };

        public override IList<BotClientCard> OnSelectCard(IList<BotClientCard> _cards, int min, int max, int hint, bool cancelable)
        {
            if (BotDuel.Phase == DuelPhase.BattleStart)
                return null;
            if (AI.HaveSelectedCards())
                return null;

            List<BotClientCard> selected = new();
            List<BotClientCard> cards = new(_cards);
            if (max > cards.Count)
                max = cards.Count;

            if (DHintForEnemy.Contains((DHint)hint))
            {
                var enemyCards = cards.Where(card => card.Controller == 1).ToList();

                // select enemy's card first
                while (enemyCards.Count > 0 && selected.Count < max)
                {
                    var card = enemyCards[UnityEngine.Random.Range(0, enemyCards.Count)];
                    selected.Add(card);
                    enemyCards.Remove(card);
                    cards.Remove(card);
                }
            }
            
            if (DHintForMaxSelect.Contains((DHint)hint))
            {
                // select max cards
                while (selected.Count < max)
                {
                    var card = cards[UnityEngine.Random.Range(0, cards.Count)];
                    selected.Add(card);
                    cards.Remove(card);
                }
            }
            return selected;
        }
        public override bool OnSelectHand()
        {
            // go first
            return true;
        }
        public bool monsterset()
        {
            if (BotDuel.Turn == 1)
            {
                return true;
            }
            else if (Bot.HasInHand(new[] {
                CardId.巨身多角龙,
                CardId.超级恐龙王,
                }))
                return false;
            return true;
        }

        private bool 抑龙Summon()
        {
            if (BotDuel.Turn == 1) return false;
                if (Enemy.GetHandCount() > 1)  return true;
            return false;
        }
        private bool 成金恐龙王Summon()
        {
            if (Enemy.GetHandCount() < 1) return true;            
            return false;
        }

        private bool 成金恐龙王Effect()
        {           
            AI.SelectCard(CardId.成金哥布林);
            return true;
        }
        private bool 奇迹的共进化Effect()
        {
           if (Bot.HasInHand(CardId.超级恐龙王))
          {
                if (Bot.HasInMonstersZone(CardId.大恐龙驾))
            {
                return false;
            }
            else
                AI.SelectCard(
                    CardId.大恐龙驾L,
                    CardId.大恐龙驾R,
                    CardId.抑龙,
                    CardId.名流雷龙,
                    CardId.荒野盗龙,
                    CardId.巨身多角龙
                    );
                AI.SelectNextCard(CardId.超级恐龙王);           
            return true;
            }
            return false;
        }
        private bool 机械镰刀盗龙Effect()
        {
            if (Bot.GetCountCardInZone(Bot.Hand, CardId.大恐龙驾) >= 2) AI.SelectCard(CardId.大恐龙驾);
            else
            if (Bot.GetCountCardInZone(Bot.Hand, CardId.大恐龙驾L) >= 2) AI.SelectCard(CardId.大恐龙驾L);
            else
            if (Bot.GetCountCardInZone(Bot.Hand, CardId.大恐龙驾R) >= 2) AI.SelectCard(CardId.大恐龙驾R);
            else
            if (Bot.GetCountCardInZone(Bot.Hand, CardId.超级恐龙王) >= 2) AI.SelectCard(CardId.超级恐龙王);
            else
                AI.SelectCard(
                    CardId.抑龙,
                    CardId.名流雷龙,
                    CardId.荒野盗龙,
                    CardId.巨身多角龙,
                    CardId.成金恐龙王,
                    CardId.机械镰刀盗龙
                    );
            return true;
        }
        private bool 恐龙粉碎Effect()
        {
            BotClientCard check = null;
            var spells = Enemy.GetSpells(); var mons = Enemy.GetMonsters();
            foreach (var spell in spells) foreach (var mon in mons)
                {
                    if (Bot.HasInMonstersZone(CardId.大恐龙驾) && mon.IsFacedown()) check = mon;
                    else if (Bot.HasInMonstersZone(CardId.大恐龙驾) && spell.IsFacedown()) check = spell;
                    else if (spell.IsFacedown()) check = spell;
                    else check = mon;
                }

            if (Bot.GetCountCardInZone(Bot.Hand, CardId.侏罗纪世界) >= 2)
            {
                AI.SelectCard(CardId.侏罗纪世界); AI.SelectNextCard(check);
            }
            else if (Bot.GetCountCardInZone(Bot.Hand, CardId.大恐龙驾) >= 2)
            {
                AI.SelectCard(CardId.大恐龙驾); AI.SelectNextCard(check);
            }
            else if (Bot.GetCountCardInZone(Bot.Hand, CardId.大恐龙驾R) >= 2)
            {
                AI.SelectCard(CardId.大恐龙驾R); AI.SelectNextCard(check);
            }
            else if (Bot.GetCountCardInZone(Bot.Hand, CardId.大恐龙驾L) >= 2)
            {
                AI.SelectCard(CardId.大恐龙驾L); AI.SelectNextCard(check);
            }
            else if (Bot.GetCountCardInZone(Bot.Hand, CardId.巨身多角龙) >= 2)
            {
                AI.SelectCard(CardId.巨身多角龙); AI.SelectNextCard(check);
            }
            else
                AI.SelectCard(                   
                    CardId.成金恐龙王,CardId.抑龙,CardId.奇迹的共进化,CardId.名流雷龙,CardId.超级恐龙王,CardId.恐力重压,CardId.超越进化,CardId.恐龙粉碎,CardId.荒野盗龙);
                AI.SelectNextCard(check);
            return true;
        }
        private bool 超越进化Effect()
        {
            AI.SelectCard(
                CardId.大恐龙驾,
                CardId.超级恐龙王
                );
            return true;
        }
        private bool 超级恐龙王Effect()
        {
            AI.SelectCard(
                   CardId.巨身多角龙,
                   CardId.荒野盗龙,
                   CardId.名流雷龙
                   );
            return true;
        }
        private bool MAXLsummon()
        {
            if (Bot.GetCountCardInZone(Bot.Hand, CardId.大恐龙驾L) >= 2 && Bot.GetCountCardInZone(Bot.Hand, CardId.奇迹的共进化) >= 1 && Bot.GetCountCardInZone(Bot.Hand, CardId.超级恐龙王) >= 1)
                return true;
            return false;
        }
        private bool MAXRsummon()
        {
            if (Bot.GetCountCardInZone(Bot.Hand, CardId.大恐龙驾R) >= 2 && Bot.GetCountCardInZone(Bot.Hand, CardId.奇迹的共进化) >= 1 && Bot.GetCountCardInZone(Bot.Hand, CardId.超级恐龙王) >= 1)
                return true;
            return false;
        }
        private bool MAXLset()
        {
            if (Bot.GetCountCardInZone(Bot.Hand, CardId.大恐龙驾L) < 2) return false;
            return true;
        }
        private bool MAXRset()
        {
            if (Bot.GetCountCardInZone(Bot.Hand, CardId.大恐龙驾R) < 2) return false;
            return true;
        }


    }
}





