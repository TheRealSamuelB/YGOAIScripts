using Enumerator;
namespace DuelBot.Game.AI.Decks
{
    public class MokeyMokeyExecutor : DefaultExecutor
    {
        public static void Init()
        {
            DecksManager.AddDeckType("Mokey Mokey", DuelRules.MasterDuel, (ai, duel) => new MokeyMokeyExecutor(ai, duel),
                "02BIaI9mguGYin0MMLzGqp8JhjX7d7HA8ON8dgYYjrB+yQrDvRNMmGHYquINIwwnJC+A44s+F1lguHdKBcPPn9UMBQHLmYKeW7LErVKC49nuZgwwDAA=");
        }
        public class CardId
        {
            // Monster Cards
            public const int MysticalShineBall = 39552864;
            public const int OjamaGreen = 12482652;
            public const int OjamaYellow = 42941100;
            public const int OjamaBlack = 79335209;
            public const int WaterSpirit = 0047395;
            public const int Kozaky = 99171160;
            public const int Gigobyte = 53776525;
            public const int SkullServant = 32274490;
            public const int MokeyMokey = 27288416;
            public const int SmokeBall = 80825553;
            public const int LeftArm = 07902349;
            public const int RightLeg = 08124921;
            public const int LeftLeg = 44519536;
            public const int RightArm = 70903634;
            public const int Bunilla = 69380702;
            public const int WhiteDuston = 03557275;

            // Trap Cards

            // Spell Cards

        }

        public MokeyMokeyExecutor(GameAI ai, BotDuel duel)
            : base(ai, duel)
        {
            AddExecutor(ExecutorType.Summon);
            AddExecutor(ExecutorType.Repos, DefaultMonsterRepos);
            AddExecutor(ExecutorType.SpellSet);
        }
    }
}
