using Enumerator;
using System;

namespace DuelBot.Game.AI.Decks
{

    public class TribrigadeExecutor : DefaultExecutor
    {
        public static void Init()
        {
            DecksManager.AddDeckType("Tribrigade", DuelRules.MasterDuel, (ai, duel) => new TribrigadeExecutor(ai, duel),
                "s+Bfr3yPAYbtlhQybjI6wwrDiWHpzDB86HgRIww3iq1jVvKYyQDD9rxSTDB8e2o2GJ/YYsQKw+F+7MwgrFgbCceRYo8YYLjbupAJhuXMmxg/XtzHNLthM8t+85lgfMRFkfXY2lfMh/wdGaYrprCWeb9l6lWfway0/DzTq1UzGUGYpUmOWXbrR1YQ/qORz+IcUc60YdF6hr+dc1hh+LrUfEYY3nSSkwWGf7NUMMGw8rW5cAwKEwA=");
        }
        public class CardId
        {
            // Monster Cards
            public const int AshBlossomJoyousSpring = 14558127;
            public const int EffectVeiler = 97268402;
            public const int TriBrigadeFraktall = 87209160;
            public const int TriBrigadeKerass = 50810455;
            public const int TriBrigadeKitt = 56196385;
            public const int TriBrigadeNervall = 14816857;
            public const int ZoodiacRatpiep = 78872731;
            public const int ZoodiacThoroughblade = 77150143;
            
            // Spell Cards
            public const int CalledByTheGrave = 24224830;
            public const int FireFormationTenki = 57103969;
            public const int ForbiddenDroplet = 24299458;
            public const int PotOfDesires = 35261759;
            public const int TripleTacticsTalent = 25311006;
            public const int ZooodiacBarrage = 46060017;

            // Trap Cards
            public const int ImperialOrder = 61740673;
            public const int InfiniteImpermanence = 10045474;
            public const int SolemnStrike = 40605147;
            public const int TriBrigadeRevolt = 40975243;

            // Extra Cards
            public const int AccesscodeTalker = 86066372;
            public const int AWODoubleDragonLords = 6671158;
            public const int BowOfTheGoddess = 04280258;
            public const int DivineArsenalSkyThunder = 90448279;
            public const int HraesvelgtDoomEagle = 49105782;
            public const int SalamangreatAlmiraj = 60303245;
            public const int TriBrigadeRampager = 47163170;
            public const int TriBrigadeBarrenBlossom = 26847978;
            public const int TriBrigadeSilverSheller = 52331012;
            public const int TriBrigadeOminousOmen = 99726621;
            public const int ZoodiacBoarbow = 74393852;
            public const int ZoodiacChakanine = 41375811;
            public const int ZoodiacTigermortar = 11510448;

            // Side Cards
            public const int DrollLockBird = 94145021;
            public const int NibiruThePrimalBeing = 27204311;
            public const int SkullMeister = 67750322;
            public const int SolemnJudgment = 41420027;
            public const int TwinTwisters = 43898403;

        }

        public TribrigadeExecutor(GameAI ai, BotDuel duel)
            : base(ai, duel)
        {
            // Monster Cards
            //AddExecutor(ExecutorType.Activate, CardId., Default);

            // Spell Cards

            // Trap Cards

            // Extra Cards

            // Side Cards

            // Common
            AddExecutor(ExecutorType.MonsterSet);
            AddExecutor(ExecutorType.SpSummon);
            AddExecutor(ExecutorType.Repos, DefaultMonsterRepos);
            AddExecutor(ExecutorType.SpellSet);

        }

        private bool InHand(int id) { return Bot.HasInHand(id); }
        private bool InGraveyard(int id) { return Bot.HasInGraveyard(id); }
        private bool InMonsterZone(int id) { return Bot.HasInMonstersZone(id); }
        private bool IsBanished(int id) { return Bot.HasInBanished(id); }

        public override bool OnSelectHand()
        {
            // go first
            return true;
        }

        private bool Default()
        {
            throw new NotImplementedException();
        }

    }
}
