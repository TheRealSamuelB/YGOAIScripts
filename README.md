![DU-Black](https://user-images.githubusercontent.com/11585545/136533996-2fd482f1-e81b-4252-835c-c26210ab0d4a.png)

# Duelists Unite - YGO Omega AI Project.

### To Learn More
* Visit our website @ https://duelistsunite.org/.
* Join our Discord Server @ https://discord.gg/duelistsunite
* Watch our Twitch @ https://www.twitch.tv/duelists_unite

### To Contribute
* You need to be registered on our [Duelists Unite Forum](https://forum.duelistsunite.org/) 
* You need to join our [Duelists Unite Discord](https://discord.gg/duelistsunite)
* Accept our **Code of Conduct**
* Agree to our **Contributor License Agreement**

### AI Scripting

- Duelists Unite Omega Custom Duel Bot API

- [WindBot AI](https://gitlab.com/duelists-unite/YGOAIScripts/-/blob/master/WindBotAI.md)

### Code of Conduct

Please find out Code of Conduct @ https://gitlab.com/duelists-unite/YGOAIScripts/-/blob/master/CODE-OF-CONDUCT.md

### Pull Request Standards

Each “significant” GitLab checkin have one of the following prefixes:
* FIX - a change that fixed a bug (we define bug as something that prevents normal / typical use)
* PERF - a change that improved performance
* UX - a change that modified the user interface or text copy
* SECURITY - a change that fixes a security problem
* FEATURE - a change that adds a new feature
* A11Y - a change that improves accessibility
* DEV - a change that modifies internals, without falling into one of the above categories
* UPDATE - a change that fixes a simple text/link/code

### Credits
* Kunogi - Creating Duelists Unite Omega Custom Duel Bot API

### Contributers

The Contributors to the **Duelists Unite - YGO Omega AI Project** can be found [here](https://gitlab.com/duelists-unite/YGOAIScripts/-/graphs/master)
